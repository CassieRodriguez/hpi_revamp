import ConfigParser
from datetime import datetime
import dateutil.relativedelta
import MySQLdb
import logging
check_table_existance = __import__('1_monthly_hpi').check_table_existance

#####################################################################################################
# Script has one function:                                                                          #
#      -szhpi_higher_geo_state  :   creating monthly state tables for both monthly and annually     #
#                                   based on table data szhpi_monthly_history                       #
#                                                                                                   #
#   run_date        :   20180130    -   Latest DPS date                                             #
#   last_dps_ym     :   201712      -   Last DPS year and month                                     #
#   last3month_ym   :   201711      -   year and second previous month from rundate                 #
#   last_dps_year   :   2017        -   Last DPS year                                               #
#####################################################################################################


def szhpi_higher_geo_state():
    logging.basicConfig(filename='hpi_logs.log', level=logging.DEBUG, format='%(asctime)s %(levelname)s : %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.info("Started Executing '7_szhpi_higher_geo_state.py' script")
    configparser = ConfigParser.RawConfigParser()
    config_filepath = r'../docs/credentials.cfg'
    configparser.read(config_filepath)

    user = configparser.get('aws2-config', 'user')
    pswd = configparser.get('aws2-config', 'pswd')
    host = configparser.get('aws2-config', 'hostdps')

    # DB Names
    databs_hpis = configparser.get('aws2-config', 'databs_hpis')

    conn = MySQLdb.connect(host=host, user=user, passwd=pswd)
    cursor = conn.cursor()
    logging.captureWarnings(cursor)

    # Date Manipulations
    now = datetime.now()
    run_datetime = now + dateutil.relativedelta.relativedelta(months=-1)
    run_date = run_datetime.strftime('%Y%m30')
    last_dps_datetime = now + dateutil.relativedelta.relativedelta(months=-2)
    last_dps_date = last_dps_datetime.strftime('%Y%m30')
    last_dps_ym = last_dps_datetime.strftime('%Y%m')
    last3month_datetime = now + dateutil.relativedelta.relativedelta(months=-3)
    last3month_ym = last3month_datetime.strftime('%Y%m')
    last_dps_year = last_dps_datetime.strftime('%Y')

    # Monthly Start
    drop_mon_hist = "DROP TABLE IF EXISTS " + databs_hpis + ".szhpi_monthly_history_state_" + run_date + ";"
    cursor.execute(drop_mon_hist)
    conn.commit()

    check_table_existance("szhpi_monthly_history_state_" + last_dps_date, databs_hpis, cursor, logging)
    create_mon_hist = """CREATE TABLE """ + databs_hpis + """.szhpi_monthly_history_state_""" + run_date + """
                (PRIMARY KEY (state_fips))
                SELECT * FROM """ + databs_hpis + """.szhpi_monthly_history_state_""" + last_dps_date + """;"""
    cursor.execute(create_mon_hist)
    conn.commit()
    logging.info("Table 'szhpi_monthly_history_state_" + run_date + "' created '" + databs_hpis + "' in Database")

    check_col = """SELECT * FROM information_schema.COLUMNS 
                WHERE 
                TABLE_SCHEMA = '""" + databs_hpis + """' 
                AND TABLE_NAME = 'szhpi_monthly_history_state_""" + run_date + """' 
                AND COLUMN_NAME = 'val_""" + last_dps_ym + """';"""
    cursor.execute(check_col)
    if cursor.fetchone() is not None:
        drop_col_mon_hist = """ALTER TABLE """ + databs_hpis + """.szhpi_monthly_history_state_""" + run_date + """
                DROP COLUMN val_""" + last_dps_ym + """;"""
        cursor.execute(drop_col_mon_hist)
        conn.commit()
        logging.info("Table 'szhpi_monthly_history_state_" + run_date + "' dropped cols")

    alter_mon_hist = """ALTER TABLE """ + databs_hpis + """.szhpi_monthly_history_state_""" + run_date + """
                ADD COLUMN val_""" + last_dps_ym + """ decimal(12,8) DEFAULT NULL AFTER val_""" + last3month_ym + """;"""
    cursor.execute(alter_mon_hist)
    conn.commit()
    logging.info("Table 'szhpi_monthly_history_state_" + run_date + "' altered")

    alter_col_mon_hist = """UPDATE """ + databs_hpis + """.szhpi_monthly_history_state_""" + run_date + """
                SET prop_count = NULL and median_price = NULL;"""
    cursor.execute(alter_col_mon_hist)
    conn.commit()
    logging.info("Table 'szhpi_monthly_history_state_" + run_date + "' altered cols")

    add_col_mon_hist = """UPDATE """ + databs_hpis + """.szhpi_monthly_history_state_""" + run_date + """ d
                INNER JOIN
                ( SELECT state_fips, AVG(val_""" + last_dps_ym + """) val_""" + last_dps_ym + """
                FROM """ + databs_hpis + """.szhpi_monthly_history
                GROUP BY state_fips, state_name) s ON
                d.state_fips = s.state_fips 
                SET d.val_""" + last_dps_ym + """ = s.val_""" + last_dps_ym + """;"""
    cursor.execute(add_col_mon_hist)
    conn.commit()
    logging.info("Table 'szhpi_monthly_history_state_" + run_date + "' added column")

    update_mon_hist = """UPDATE """ + databs_hpis + """.szhpi_monthly_history_state_""" + run_date + """ t, geo_data.geo_states s
                SET t.prop_count=s.prop_count
                WHERE t.state_fips=s.id;"""
    cursor.execute(update_mon_hist)
    conn.commit()
    logging.info("Table 'szhpi_monthly_history_state_" + run_date + "' updated")
    # Monthly End

    # Annual Start
    drop_annual_hist = "DROP TABLE IF EXISTS " + databs_hpis + ".szhpi_annual_history_state_" + run_date + ";"
    cursor.execute(drop_annual_hist)
    conn.commit()

    check_table_existance("szhpi_annual_history_state_" + last_dps_date, databs_hpis, cursor, logging)
    create_annual_hist = """CREATE TABLE """ + databs_hpis + """.szhpi_annual_history_state_""" + run_date + """
                (PRIMARY KEY (state_fips))
                SELECT * FROM """ + databs_hpis + """.szhpi_annual_history_state_""" + last_dps_date + """;"""
    cursor.execute(create_annual_hist)
    conn.commit()
    logging.info("Table 'szhpi_annual_history_state_" + run_date + "' created '" + databs_hpis + "' in Database")

    check_col = """SELECT * FROM information_schema.COLUMNS 
                WHERE 
                TABLE_SCHEMA = '""" + databs_hpis + """' 
                AND TABLE_NAME = 'szhpi_annual_history_state_""" + run_date + """' 
                AND COLUMN_NAME = 'val_""" + last_dps_year + """';"""
    cursor.execute(check_col)
    if cursor.fetchone() is not None:
        drop_col_annual_hist = """ALTER TABLE """ + databs_hpis + """.szhpi_annual_history_state_""" + run_date + """
                DROP COLUMN val_""" + last_dps_year + """;"""
        cursor.execute(drop_col_annual_hist)
        conn.commit()
        logging.info("Table 'szhpi_annual_history_state_" + run_date + "' dropped cols")

    alter_annual_hist = """ALTER TABLE """ + databs_hpis + """.szhpi_annual_history_state_""" + run_date + """
                ADD COLUMN val_""" + last_dps_year + """ decimal(12,8) DEFAULT NULL;"""
    cursor.execute(alter_annual_hist)
    conn.commit()
    logging.info("Table 'szhpi_annual_history_state_" + run_date + "' altered")

    alter_col_annual_hist = """UPDATE """ + databs_hpis + """.szhpi_annual_history_state_""" + run_date + """
                SET prop_count = NULL and median_price = NULL;"""
    cursor.execute(alter_col_annual_hist)
    conn.commit()
    logging.info("Table 'szhpi_annual_history_state_" + run_date + "' altered cols")

    add_col_annual_hist = """UPDATE """ + databs_hpis + """.szhpi_annual_history_state_""" + run_date + """ d
                INNER JOIN
                ( SELECT state_fips, AVG(val_""" + last_dps_year + """) val_""" + last_dps_year + """
                FROM """ + databs_hpis + """.szhpi_annual_history
                GROUP BY state_fips, state_name) s ON
                d.state_fips = s.state_fips 
                SET d.val_""" + last_dps_year + """ = s.val_""" + last_dps_year + """;"""
    cursor.execute(add_col_annual_hist)
    conn.commit()
    logging.info("Table 'szhpi_annual_history_state_" + run_date + "' added column")

    update_annual_hist = """UPDATE """ + databs_hpis + """.szhpi_annual_history_state_""" + run_date + """ t, geo_data.geo_states s
                SET t.prop_count=s.prop_count
                WHERE t.state_fips=s.id;"""
    cursor.execute(update_annual_hist)
    conn.commit()
    logging.info("Table 'szhpi_annual_history_state_" + run_date + "' updated")
    # Annual End

    # Verifying table 'szhpi_monthly_history_state_[run_date]' data
    check_null = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_history_state_""" + run_date + """
                                            WHERE state_fips IS NULL OR
                                            state_name IS NULL OR
                                            val_""" + last_dps_ym + """ IS NULL OR
                                            state_fips = '' OR
                                            state_name = '' OR
                                            val_""" + last_dps_ym + """ = '';"""
    logging.info("Test Case #1 : NULL or Empty check")
    cursor.execute(check_null)
    if cursor.fetchone()[0] != 0:
        raise Exception("Null or Empty value exists")
    else:
        logging.info("Test Case #1 : Passed")

    check_sfips = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_history_state_""" + run_date + """
                WHERE state_fips NOT IN (SELECT distinct(state_fips) FROM """ + databs_hpis + """.szhpi_monthly_history_state_""" + last_dps_date + """);"""
    logging.info("Test Case #2 : 'state_fips' values check")
    cursor.execute(check_sfips)
    if cursor.fetchone()[0] != 0:
        raise Exception("Wrong 'state_fips' value exists in 'szhpi_monthly_history_state_" + run_date + "' table")
    else:
        logging.info("Test Case #2 : Passed")

    check_sname = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_history_state_""" + run_date + """
                WHERE state_name NOT IN (SELECT distinct(state_name) FROM """ + databs_hpis + """.szhpi_monthly_history_state_""" + last_dps_date + """);"""
    logging.info("Test Case #3 : 'state_name' values check")
    cursor.execute(check_sname)
    if cursor.fetchone()[0] != 0:
        raise Exception("Wrong 'state_name' value exists in 'szhpi_monthly_history_state_" + run_date + "' table")
    else:
        logging.info("Test Case #3 : Passed")

    check_prop = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_history_state_""" + run_date + """ t, geo_data.geo_states s
                    WHERE t.state_fips = s.id AND t.prop_count != s.prop_count;"""
    logging.info("Test Case #4 : 'prop_count' values check")
    cursor.execute(check_prop)
    if cursor.fetchone()[0] != 0:
        raise Exception("Wrong 'prop_count' value exists in 'szhpi_monthly_history_state_" + run_date + "' table")
    else:
        logging.info("Test Case #4 : Passed")

    check_val = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_history_state_""" + run_date + """
                                    WHERE val_""" + last_dps_ym + """ NOT BETWEEN 1 AND 500 OR val_""" + last_dps_ym + """ REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #5 : 'val_" + last_dps_ym + "' values check")
    cursor.execute(check_val)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid 'val_" + last_dps_ym + "' value exists")
    else:
        logging.info("Test Case #5 : Passed")

    # Verifying table 'szhpi_annual_history_state_[run_date]' data
    check_null = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_annual_history_state_""" + run_date + """
                                            WHERE state_fips IS NULL OR
                                            state_name IS NULL OR
                                            val_""" + last_dps_year + """ IS NULL OR
                                            state_fips = '' OR
                                            state_name = '' OR
                                            val_""" + last_dps_year + """ = '';"""
    logging.info("Test Case #6 : NULL or Empty values check")
    cursor.execute(check_null)
    if cursor.fetchone()[0] != 0:
        raise Exception("Null or Empty value exists")
    else:
        logging.info("Test Case #6 : Passed")

    check_sfips = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_annual_history_state_""" + run_date + """
                WHERE state_fips NOT IN ( SELECT distinct(state_fips) FROM """ + databs_hpis + """.szhpi_annual_history_state_""" + last_dps_date + """);"""
    logging.info("Test Case #7 : 'state_fips' values check")
    cursor.execute(check_sfips)
    if cursor.fetchone()[0] != 0:
        raise Exception("Wrong 'state_fips' value exists in 'szhpi_annual_history_state_" + run_date + "' table")
    else:
        logging.info("Test Case #7 : Passed")

    check_sname = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_annual_history_state_""" + run_date + """
                WHERE state_name NOT IN ( SELECT distinct(state_name) FROM """ + databs_hpis + """.szhpi_annual_history_state_""" + last_dps_date + """);"""
    logging.info("Test Case #8 : 'state_name' values check")
    cursor.execute(check_sname)
    if cursor.fetchone()[0] != 0:
        raise Exception("Wrong 'state_name' value exists in 'szhpi_annual_history_state_" + run_date + "' table")
    else:
        logging.info("Test Case #8 : Passed")

    check_prop = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_annual_history_state_""" + run_date + """ t, geo_data.geo_states s
                    WHERE t.state_fips = s.id AND t.prop_count != s.prop_count;"""
    logging.info("Test Case #9 : 'prop_count' values check")
    cursor.execute(check_prop)
    if cursor.fetchone()[0] != 0:
        raise Exception("Wrong 'prop_count' value exists in 'szhpi_annual_history_state_" + run_date + "' table")
    else:
        logging.info("Test Case #9 : Passed")

    check_val = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_annual_history_state_""" + run_date + """
                                    WHERE val_""" + last_dps_year + """ NOT BETWEEN 1 AND 500 OR val_""" + last_dps_year + """ REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #10 : 'val_" + last_dps_year + "' values check")
    cursor.execute(check_val)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid 'val_" + last_dps_year + "' value exists")
    else:
        logging.info("Test Case #10 : Passed")

    logging.info("Verification of output tables completed")
    conn.close()

    logging.info("Completed Execution of '7_szhpi_higher_geo_state.py' script")

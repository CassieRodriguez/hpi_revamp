import ConfigParser
from datetime import datetime
import dateutil.relativedelta
import MySQLdb
import logging
check_table_existance = __import__('1_monthly_hpi').check_table_existance

#####################################################################################################
# Script has two functions:                                                                         #
#      -szhpi_higher_geo    :   creating monthly geo tables for both monthly and annually based on  #
#                               table data szhpi_monthly_history                                    #
#      -patch               :   updating geoid and name using table geo_data for place geo          #
#                                                                                                   #
#   run_date        :   20180130    -   Latest DPS date                                             #
#   last_dps_ym     :   201712      -   Last DPS year and month                                     #
#   last3month_ym   :   201711      -   year and second previous month from rundate                 #
#   last_dps_year   :   2017        -   Last DPS year                                               #
#####################################################################################################

logging.basicConfig(filename='hpi_logs.log', level=logging.DEBUG, format='%(asctime)s %(levelname)s : %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
configparser = ConfigParser.RawConfigParser()
config_filepath = r'../docs/credentials.cfg'
configparser.read(config_filepath)

user = configparser.get('aws2-config', 'user')
pswd = configparser.get('aws2-config', 'pswd')
host = configparser.get('aws2-config', 'hostdps')

# DB Names
databs_hpis = configparser.get('aws2-config', 'databs_hpis')

# Date Manipulations
now = datetime.now()
run_datetime = now + dateutil.relativedelta.relativedelta(months=-1)
run_date = run_datetime.strftime('%Y%m30')
last_dps_datetime = now + dateutil.relativedelta.relativedelta(months=-2)
last_dps_date = last_dps_datetime.strftime('%Y%m30')
last_dps_ym = last_dps_datetime.strftime('%Y%m')
last3month_datetime = now + dateutil.relativedelta.relativedelta(months=-3)
last3month_ym = last3month_datetime.strftime('%Y%m')
last_dps_year = last_dps_datetime.strftime('%Y')


def szhpi_higher_geo():
    logging.info("Started Executing 'szhpi_higher_geo' function of '6_szhpi_higher_geo.py script'")

    conn = MySQLdb.connect(host=host, user=user, passwd=pswd)
    cursor = conn.cursor()
    logging.captureWarnings(cursor)

    for GEO in ['cbsa', 'place', 'county']:
        # Monthly Start
        # Getting Geo_Data table
        drop_mon_hist = "DROP TABLE IF EXISTS " + databs_hpis + ".szhpi_monthly_history_" + GEO + "_" + run_date + ";"
        cursor.execute(drop_mon_hist)
        conn.commit()

        check_table_existance("szhpi_monthly_history_" + GEO + "_" + last_dps_date, databs_hpis, cursor, logging)
        create_mon_hist = """CREATE TABLE """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + run_date + """
                    (PRIMARY KEY (""" + GEO + """_geoid))
                    SELECT * FROM """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + last_dps_date + """;"""
        cursor.execute(create_mon_hist)
        conn.commit()
        logging.info("Table 'szhpi_monthly_history_" + GEO + "_" + run_date + "' created")

        check_col = """SELECT * FROM information_schema.COLUMNS 
                    WHERE 
                    TABLE_SCHEMA = '""" + databs_hpis + """' 
                    AND TABLE_NAME = 'szhpi_monthly_history_""" + GEO + """_""" + run_date + """' 
                    AND COLUMN_NAME = 'val_""" + last_dps_ym + """';"""
        cursor.execute(check_col)
        if cursor.fetchone() is not None:
            drop_col_mon_hist = """ALTER TABLE """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + run_date + """
                    DROP COLUMN val_""" + last_dps_ym + """;"""
            cursor.execute(drop_col_mon_hist)
            conn.commit()
            logging.info("Table 'szhpi_monthly_history_" + GEO + "_" + run_date + "' dropped cols")

        alter_mon_hist = """ALTER TABLE """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + run_date + """
                    ADD COLUMN val_""" + last_dps_ym + """ decimal(12,8) DEFAULT NULL AFTER val_""" + last3month_ym + """;"""
        cursor.execute(alter_mon_hist)
        conn.commit()
        logging.info("Table 'szhpi_monthly_history_" + GEO + "_" + run_date + "' altered")

        alter_col_mon_hist = """UPDATE """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + run_date + """
                    SET prop_count = NULL and median_price = NULL;"""
        cursor.execute(alter_col_mon_hist)
        conn.commit()
        logging.info("Table 'szhpi_monthly_history_" + GEO + "_" + run_date + "' altered cols")

        add_col_mon_hist = """UPDATE """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + run_date + """ d
                    INNER JOIN
                    ( SELECT """ + GEO + """_geoid, AVG(val_""" + last_dps_ym + """) val_""" + last_dps_ym + """
                    FROM """ + databs_hpis + """.szhpi_monthly_history
                    WHERE """ + GEO + """_geoid>0
                    GROUP BY """ + GEO + """_geoid, """ + GEO + """_name) s ON
                    s.""" + GEO + """_geoid = d.""" + GEO + """_geoid
                    SET d.val_""" + last_dps_ym + """ = s.val_""" + last_dps_ym + """;"""
        cursor.execute(add_col_mon_hist)
        conn.commit()
        logging.info("Table 'szhpi_monthly_history_" + GEO + "_" + run_date + "' added column")

        update_mon_hist = """UPDATE """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + run_date + """ t, geo_data.geo_""" + (GEO if GEO != 'county' else 'countie') + """s s
                    SET t.prop_count=s.prop_count
                    WHERE t.""" + GEO + """_geoid=s.id;"""
        cursor.execute(update_mon_hist)
        conn.commit()
        logging.info("Table 'szhpi_monthly_history_" + GEO + "_" + run_date + "' updated")
        # Monthly End

        # Annual Start
        drop_annual_hist = "DROP TABLE IF EXISTS " + databs_hpis + ".szhpi_annual_history_" + GEO + "_" + run_date + ";"
        cursor.execute(drop_annual_hist)
        conn.commit()

        check_table_existance("szhpi_annual_history_" + GEO + "_" + last_dps_date, databs_hpis, cursor, logging)
        create_annual_hist = """CREATE TABLE """ + databs_hpis + """.szhpi_annual_history_""" + GEO + """_""" + run_date + """
                    (PRIMARY KEY (""" + GEO + """_geoid))
                    SELECT * FROM """ + databs_hpis + """.szhpi_annual_history_""" + GEO + """_""" + last_dps_date + """;"""
        cursor.execute(create_annual_hist)
        conn.commit()
        logging.info("Table 'szhpi_annual_history_" + GEO + "_" + run_date + "' created")

        check_col = """SELECT * FROM information_schema.COLUMNS 
                    WHERE 
                    TABLE_SCHEMA = '""" + databs_hpis + """' 
                    AND TABLE_NAME = 'szhpi_annual_history_""" + GEO + """_""" + run_date + """' 
                    AND COLUMN_NAME = 'val_""" + last_dps_year + """';"""
        cursor.execute(check_col)
        if cursor.fetchone() is not None:
            drop_col_annual_hist = """ALTER TABLE """ + databs_hpis + """.szhpi_annual_history_""" + GEO + """_""" + run_date + """
                    DROP COLUMN val_""" + last_dps_year + """;"""
            cursor.execute(drop_col_annual_hist)
            conn.commit()
            logging.info("Table 'szhpi_annual_history_" + GEO + "_" + run_date + "' dropped cols")

        alter_annual_hist = """ALTER TABLE """ + databs_hpis + """.szhpi_annual_history_""" + GEO + """_""" + run_date + """
                    ADD COLUMN val_""" + last_dps_year + """ decimal(27,12) DEFAULT NULL;"""
        cursor.execute(alter_annual_hist)
        conn.commit()
        logging.info("Table 'szhpi_annual_history_" + GEO + "_" + run_date + "' altered")

        add_col_annual_hist = """UPDATE """ + databs_hpis + """.szhpi_annual_history_""" + GEO + """_""" + run_date + """ d
                    INNER JOIN
                    ( SELECT """ + GEO + """_geoid, AVG(val_""" + last_dps_year + """) val_""" + last_dps_year + """
                    FROM """ + databs_hpis + """.szhpi_annual_history
                    WHERE """ + GEO + """_geoid > 0
                    GROUP BY """ + GEO + """_geoid, """ + GEO + """_name ) s ON
                    d.""" + GEO + """_geoid = s.""" + GEO + """_geoid 
                    SET d.val_""" + last_dps_year + """ = s.val_""" + last_dps_year + """;"""
        cursor.execute(add_col_annual_hist)
        conn.commit()
        logging.info("Table 'szhpi_annual_history_" + GEO + "_" + run_date + "' added column")

        update_annual_hist = """UPDATE """ + databs_hpis + """.szhpi_annual_history_""" + GEO + """_""" + run_date + """ t, geo_data.geo_""" + (GEO if GEO != 'county' else 'countie') + """s s
                    SET t.prop_count=s.prop_count
                    WHERE t.""" + GEO + """_geoid=s.id;"""
        cursor.execute(update_annual_hist)
        conn.commit()
        logging.info("Table 'szhpi_annual_history_" + GEO + "_" + run_date + "' updated")
        # Annual End

        logging.info("Verification of output tables started")
        # Verifying table 'szhpi_monthly_history_[geo]_[run_date]' data
        check_null = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + run_date + """
                                WHERE state_fips IS NULL OR
                                state_name IS NULL OR
                                state_fips = '' OR
                                state_name = '';"""
        logging.info("Test Case #1 : NULL or Empty values check")
        cursor.execute(check_null)
        if cursor.fetchone()[0] != 0:
            raise Exception("Null or Empty value exists")
        else:
            logging.info("Test Case #1 : Passed")

        check_geo = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + run_date + """
                                WHERE """ + GEO + """_geoid NOT REGEXP '[0-9]' OR """ + GEO + """_geoid REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
        logging.info("Test Case #2 : '" + GEO + "_geoid' values check")
        cursor.execute(check_geo)
        if cursor.fetchone()[0] != 0:
            raise Exception("Invalid '" + GEO + "_geoid' value exists")
        else:
            logging.info("Test Case #2 : Passed")

        check_geoname = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + run_date + """
                                WHERE """ + GEO + """_name REGEXP '[0-9~`!@#$%^&*_=+{}|\:;<>?]';"""
        logging.info("Test Case #3 : " + GEO + "_name values check")
        cursor.execute(check_geoname)
        if cursor.fetchone()[0] != 0:
            raise Exception("Invalid '" + GEO + "_name' value exists")
        else:
            logging.info("Test Case #3 : Passed")

        check_sfips = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + run_date + """
                                WHERE state_fips NOT IN (select distinct(state_fips) from """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + last_dps_date + """);"""
        logging.info("Test Case #4 : 'state_fips' values check")
        cursor.execute(check_sfips)
        if cursor.fetchone()[0] != 0:
            raise Exception("Invalid 'state_fips' value exists")
        else:
            logging.info("Test Case #4 : Passed")

        check_sname = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + run_date + """
                                WHERE state_name NOT IN (select distinct(state_name) from """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + last_dps_date + """);"""
        logging.info("Test Case #5 : 'state_name' values check")
        cursor.execute(check_sname)
        if cursor.fetchone()[0] != 0:
            raise Exception("Invalid 'state_name' value exists")
        else:
            logging.info("Test Case #5 : Passed")

        check_val = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + run_date + """
                                WHERE val_""" + last_dps_ym + """ NOT BETWEEN 1 AND 500 OR val_""" + last_dps_ym + """ REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
        logging.info("Test Case #6 : 'val_""" + last_dps_ym + "' values check")
        cursor.execute(check_val)
        if cursor.fetchone()[0] != 0:
            raise Exception("Invalid 'val_" + last_dps_ym + "' value exists")
        else:
            logging.info("Test Case #6 : Passed")

        check_prop = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + run_date + """ t,
                                geo_data.geo_""" + (GEO if GEO != 'county' else 'countie') + """s s WHERE t.""" + GEO + """_geoid = s.id and t.prop_count != s.prop_count;"""
        logging.info("Test Case #7 : 'prop_count' values check")
        cursor.execute(check_prop)
        if cursor.fetchone()[0] != 0:
            raise Exception("Wrong 'prop_count' value exists")
        else:
            logging.info("Test Case #7 : Passed")

        # Verifying table 'szhpi_annual_history_[geo]_[run_date]' data
        check_null = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_annual_history_""" + GEO + """_""" + run_date + """
                                        WHERE state_fips IS NULL OR
                                        state_name IS NULL OR
                                        state_fips = '' OR
                                        state_name = '';"""
        logging.info("Test Case #8 : NULL or Empty values check")
        cursor.execute(check_null)
        if cursor.fetchone()[0] != 0:
            raise Exception("Null or Empty value exists")
        else:
            logging.info("Test Case #8 : Passed")

        check_geo = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_annual_history_""" + GEO + """_""" + run_date + """
                                        WHERE """ + GEO + """_geoid NOT REGEXP '[0-9]' OR """ + GEO + """_geoid REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
        logging.info("Test Case #9 : '" + GEO + "_geoid' values check")
        cursor.execute(check_geo)
        if cursor.fetchone()[0] != 0:
            raise Exception("Invalid '" + GEO + "_geoid' value exists")
        else:
            logging.info("Test Case #9 : Passed")

        check_geoname = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_annual_history_""" + GEO + """_""" + run_date + """
                                        WHERE """ + GEO + """_name REGEXP '[0-9~`!@#$%^&*_=+{}|\:;<>?]';"""
        logging.info("Test Case #10 : '" + GEO + "_name' values check")
        cursor.execute(check_geoname)
        if cursor.fetchone()[0] != 0:
            raise Exception("Invalid '" + GEO + "_name' value exists")
        else:
            logging.info("Test Case #10 : Passed")

        check_sfips = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_annual_history_""" + GEO + """_""" + run_date + """
                                        WHERE state_fips NOT IN (select distinct(state_fips) from """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + last_dps_date + """);"""
        logging.info("Test Case #11 : 'state_fips' values check")
        cursor.execute(check_sfips)
        if cursor.fetchone()[0] != 0:
            raise Exception("Invalid 'state_fips' value exists")
        else:
            logging.info("Test Case #11 : Passed")

        check_sname = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_annual_history_""" + GEO + """_""" + run_date + """
                                        WHERE state_name NOT IN (select distinct(state_name) from """ + databs_hpis + """.szhpi_monthly_history_""" + GEO + """_""" + last_dps_date + """);"""
        logging.info("Test Case #12 : 'state_name' values check")
        cursor.execute(check_sname)
        if cursor.fetchone()[0] != 0:
            raise Exception("Invalid 'state_name' value exists")
        else:
            logging.info("Test Case #12 : Passed")

        check_val = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_annual_history_""" + GEO + """_""" + run_date + """
                                        WHERE val_""" + last_dps_year + """ NOT BETWEEN 1 AND 1000 OR val_""" + last_dps_year + """ REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
        logging.info("Test Case #13 : 'val_" + last_dps_year + "' values check")
        #cursor.execute(check_val)
        #if cursor.fetchone()[0] != 0:
            #raise Exception("Invalid 'val_" + last_dps_year + "' value exists")
        #else:
            #logging.info("Test Case #13 : Passed")

        check_prop = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_annual_history_""" + GEO + """_""" + run_date + """ t,
                                        geo_data.geo_""" + (
            GEO if GEO != 'county' else 'countie') + """s s WHERE t.""" + GEO + """_geoid = s.id and t.prop_count != s.prop_count;"""
        logging.info("Test Case #14 : 'prop_count' values check")
        cursor.execute(check_prop)
        if cursor.fetchone()[0] != 0:
            raise Exception("Wrong 'prop_count' value exists")
        else:
            logging.info("Test Case #14 : Passed")

        logging.info("Verification of output tables completed")
    conn.close()

    logging.info("Completed execution of 'szhpi_higher_geo' function of '6_szhpi_higher_geo.py' script")


def patch():
    logging.info("Started Executing 'patch' function of '6_szhpi_higher_geo.py script'")

    conn = MySQLdb.connect(host=host, user=user, passwd=pswd)
    cursor = conn.cursor()
    logging.captureWarnings(cursor)
    logging.info("Started updating 'szhpi_monthly_history_place_" + run_date + " table")

    update1_mon_hist_place = """UPDATE """ + databs_hpis + """.szhpi_monthly_history_place_""" + run_date + """ t, geo_data.geo_places s
                SET t.cbsa_geoid=s.geo_cbsa_id
                WHERE t.place_geoid=s.id;"""
    cursor.execute(update1_mon_hist_place)
    conn.commit()

    update2_mon_hist_place = """UPDATE """ + databs_hpis + """.szhpi_monthly_history_place_""" + run_date + """ t, geo_data.geo_cbsas s
                SET t.cbsa_name=s.name
                WHERE t.cbsa_geoid=s.id;"""
    cursor.execute(update2_mon_hist_place)
    conn.commit()

    logging.info("Started updating 'szhpi_annual_history_place_" + run_date + " table")
    update1_annual_hist_place = """UPDATE """ + databs_hpis + """.szhpi_annual_history_place_""" + run_date + """ t, geo_data.geo_places s
                SET t.cbsa_geoid=s.geo_cbsa_id
                WHERE t.place_geoid=s.id;"""
    cursor.execute(update1_annual_hist_place)
    conn.commit()

    update2_annual_hist_place = """UPDATE """ + databs_hpis + """.szhpi_annual_history_place_""" + run_date + """ t, geo_data.geo_cbsas s
                SET t.cbsa_name=s.name
                WHERE t.cbsa_geoid=s.id;"""
    cursor.execute(update2_annual_hist_place)
    conn.commit()
    logging.info("Updates completed")

    logging.info("Verification of output tables started")
    # Verifying table 'szhpi_monthly_history_place_[run_date]' data
    check_cbsaid = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_history_place_""" + run_date + """ t,
                                    geo_data.geo_places s WHERE t.place_geoid = s.id and t.cbsa_geoid != s.geo_cbsa_id;"""
    logging.info("Test Case #1 : 'cbsa_geoid' values check")
    cursor.execute(check_cbsaid)
    if cursor.fetchone()[0] != 0:
        raise Exception("Wrong 'cbsa_geoid' value exists in 'szhpi_monthly_history_place_" + run_date + "' table")
    else:
        logging.info("Test Case #1 : Passed")

    check_cbsaname = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_history_place_""" + run_date + """ t,
                                        geo_data.geo_cbsas s WHERE t.cbsa_geoid=s.id and t.cbsa_name!=s.name COLLATE latin1_swedish_ci;"""
    logging.info("Test Case #2 : 'cbsa_name' values check")
    cursor.execute(check_cbsaname)
    if cursor.fetchone()[0] != 0:
        raise Exception("Wrong 'cbsa_name' value exists in 'szhpi_monthly_history_place_" + run_date + "' table")
    else:
        logging.info("Test Case #2 : Passed")

    # Verifying table 'szhpi_annual_history_place_[run_date]' data
    check_cbsaid = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_annual_history_place_""" + run_date + """ t,
                                    geo_data.geo_places s WHERE t.place_geoid = s.id and t.cbsa_geoid != s.geo_cbsa_id;"""
    logging.info("Test Case #3 : 'cbsa_geoid' values check")
    cursor.execute(check_cbsaid)
    if cursor.fetchone()[0] != 0:
        raise Exception("Wrong 'cbsa_geoid' value exists in 'szhpi_annual_history_place_" + run_date + "' table")
    else:
        logging.info("Test Case #3 : Passed")

    check_cbsaname = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_annual_history_place_""" + run_date + """ t,
                                        geo_data.geo_cbsas s WHERE t.cbsa_geoid = s.id and t.cbsa_name != s.name COLLATE latin1_swedish_ci;"""
    logging.info("Test Case #4 : 'cbsa_name' values check")
    cursor.execute(check_cbsaname)
    if cursor.fetchone()[0] != 0:
        raise Exception("Wrong 'cbsa_name' value exists in 'szhpi_annual_history_place_" + run_date + "' table")
    else:
        logging.info("Test Case #4 : Passed")

    logging.info("Verification of output tables completed")
    conn.close()

    logging.info("Completed execution of 'patch' function of '6_szhpi_higher_geo.py' script")

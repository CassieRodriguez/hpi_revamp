##========================== monthly hpi from case shiller === line number 524 of monthly hpi script
import csv
import ConfigParser
import pandas as pd
import MySQLdb
import logging
import os
from datetime import datetime
check_table_existance = __import__('1_monthly_hpi').check_table_existance

#############################################################################################################
# Script has one function:                                                                                  #
#      -monthly_hpi_cbsas_horiz           :     creating table 'cs_cbsas_sa_horizontal'                     #
#                                                                                                           #
#   curr_year       :   18          -   Current year(only year number of 2018)                              #
#   curr_ym         :   201802      -   Current year and month when delivery happens                        #
#############################################################################################################


def monthly_hpi_cbsas_horiz():
    logging.basicConfig(filename='hpi_logs.log', level=logging.DEBUG, format='%(asctime)s %(levelname)s : %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.info("Started Executing '2_monthly_hpi_cbsas_horizontal.py' script")
    configparser = ConfigParser.RawConfigParser()
    config_filepath = r'../docs/credentials.cfg'
    configparser.read(config_filepath)

    user = configparser.get('aws2-config', 'user')
    pswd = configparser.get('aws2-config','pswd')
    host = configparser.get('aws2-config','hostdps')
    databs = configparser.get('aws2-config','databs')

    db = MySQLdb.connect(host=host, user=user, passwd=pswd, db=databs)
    cursor = db.cursor()
    logging.captureWarnings(cursor)

    now = datetime.now()
    curr_ym = now.strftime('%Y%m')

    check_table_existance("cs_" + curr_ym + "_cbsas_sa", databs, cursor, logging)
    select_cs_cbsas = "SELECT month,year,hpi,metro_name NAME,cbsa_geoid,state_code FROM " + databs + ".cs_" + curr_ym + "_cbsas_sa;"
    cursor.execute(select_cs_cbsas)
    data = cursor.fetchall()

    f = open('hpi_cbsas_sa.csv', 'w')
    for t in data:
        line = ','.join(str(x) for x in t)
        f.write(line + '\n')
    f.close()

    # Adding header to the above output
    with open('hpi_cbsas_sa.csv') as sample, open('hpi_cbsas_final.csv', 'w') as output:
        reader = csv.reader(sample)
        writer = csv.writer(output)
        writer.writerow(['Month', 'Year', 'HPI', 'NAME', 'cbsa_geoid', 'State_code'])
        for row in reader:
            if row:
                writer.writerow([row[0], row[1], row[2], row[3], row[4], row[5]])

    # Adding concatenated column
    pre_horizontal_df = pd.read_csv('hpi_cbsas_final.csv')
    pre_horizontal_df['period'] = pre_horizontal_df[['Year','Month']].dot([100,1])
    pre_horizontal_df['period'] = "val_" + pre_horizontal_df['period'].astype(str)

    # Horizontal Pivoting of the data
    horizontal_df = pre_horizontal_df.pivot_table(index=('cbsa_geoid','NAME','State_code'), columns='period', values='HPI', aggfunc='mean')
    horizontal_df = horizontal_df.reset_index().rename_axis(None, axis=1)
    # DROPPING COLUMNS BETWEEN VAL_198701 AND VAL_199912
    horizontal_df = horizontal_df.drop(horizontal_df.columns.to_series()["val_198701":"val_199912"], axis=1)

    horizontal_df.to_csv('hpi_cbsas_horiz.csv', encoding='utf-8', index=False)
    logging.info("CSV file 'hpi_cbsas_horiz.csv' created")

    end_date = curr_ym[:4] + "-" + curr_ym[4:]
    mon_cols = [i.strftime("val_%Y%m") for i in pd.date_range(start="2000-01", end=end_date, freq='MS')]  # till current month
    mon_cols = mon_cols[:-3]  # neglecting last three months using splicing
    mon_col_list = map(lambda x: x + " DEC(8,4)", mon_cols)

    drop_cs_cbsas_horiz = ("DROP TABLE IF EXISTS " + databs + ".cs_" + curr_ym + "_cbsas_sa_horizontal;")
    cursor.execute(drop_cs_cbsas_horiz)
    db.commit()

    create_cs_cbsas_horiz = """CREATE TABLE """ + databs + """.cs_""" + curr_ym + """_cbsas_sa_horizontal
                (PRIMARY KEY (cbsa_geoid),
                cbsa_geoid int (15),
                NAME varchar(60),
                state_code varchar(3),
                """ + ",".join(mon_col_list) + """);"""

    cursor.execute(create_cs_cbsas_horiz)
    db.commit()
    logging.info("Table 'cs_" + curr_ym + "_cbsas_sa_horizontal' created in '" + databs + "' Database")
    
    load_cs_cbsas_horiz = ("""LOAD DATA LOCAL INFILE
                'hpi_cbsas_horiz.csv'
                INTO TABLE
                """ + databs + ".cs_" + curr_ym + "_cbsas_sa_horizontal" + """
                CHARACTER SET utf8
                FIELDS TERMINATED BY ','
                LINES TERMINATED BY '\n'
                IGNORE 1 LINES
                ;""")
    
    cursor.execute(load_cs_cbsas_horiz)
    db.commit()
    logging.info("Load from CSV file 'hpi_cbsas_horiz.csv' to Table 'cs_" + curr_ym + "_cbsas_sa_horizontal' completed")

    os.remove("hpi_cbsas_sa.csv")
    os.remove("hpi_cbsas_final.csv")
    os.remove("hpi_cbsas_horiz.csv")
    logging.info("Removed intermediate CSV files")

    logging.info("Verification of output tables started")
    # Verifying table 'cs_[curr_ym]_cbsas_sa_horizontal' data
    check_null = """SELECT count(*) FROM """ + databs + """.cs_""" + curr_ym + """_cbsas_sa_horizontal
                WHERE cbsa_geoid IS NULL OR
                NAME  IS NULL OR
                state_code  IS NULL OR
                cbsa_geoid = '' OR
                NAME = '' OR
                state_code = '';"""
    logging.info("Test Case #1 : NULL or Empty check")
    cursor.execute(check_null)
    if cursor.fetchone()[0] != 0:
        raise Exception("NULL or Empty value exists")
    else:
        logging.info("Test Case #1 : Passed")

    check_name = """SELECT count(*) FROM """ + databs + """.cs_""" + curr_ym + """_cbsas_sa_horizontal 
                WHERE NAME REGEXP "[0-9~`!@#$%^&*_=+{}|\:;<>?/]";"""
    logging.info("Test Case #2 : 'NAME' values check")
    cursor.execute(check_name)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid 'NAME' value exists")
    else:
        logging.info("Test Case #2 : Passed")

    check_scode = """SELECT count(*) FROM """ + databs + """.cs_""" + curr_ym + """_cbsas_sa_horizontal
                        WHERE state_code NOT IN ("AL","AK","AZ","AR","CA","CO","CT","DE","DC","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY","AL","AK","AZ","AR","CA","CO","CT","DE","DC","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY");"""
    logging.info("Test Case #3 : 'state_code' values check")
    cursor.execute(check_scode)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid state_code value exists")
    else:
        logging.info("Test Case #3 : Passed")

    check_mon_list = map(lambda x: x + " NOT BETWEEN 1 AND 500", mon_cols)
    check_val = """SELECT count(*) FROM """ + databs + """.cs_""" + curr_ym + """_cbsas_sa_horizontal
                                    WHERE """ + ' OR '.join(check_mon_list) + """;"""
    logging.info("Test Case #4 : Columns (val_[#]) values check")
    cursor.execute(check_val)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid val_[#] value exists")
    else:
        logging.info("Test Case #4 : Passed")

    logging.info("Verification of output tables completed")
    db.close()

    logging.info("Completed Execution of '2_monthly_hpi_cbsas_horizontal.py' script")

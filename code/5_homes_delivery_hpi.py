#!/usr/bin/python
import ConfigParser
import MySQLdb
import logging
from datetime import datetime
import dateutil.relativedelta
check_table_existance = __import__('1_monthly_hpi').check_table_existance

#############################################################################################################
# Script has one function:                                                                                  #
#      -homes_delivery  :   creating 'szhpi_monthly_increment' table, taking backup                         #
#                           of 'szhpi_annual_history' as 'szhpi_annual_history_bkp' and updating            #
#                                                                                                           #
#   run_date        :   20180130    -   Latest DPS date                                                     #
#   last_dps_ym     :   201712      -   Last DPS year and month                                             #
#   last_dps_year   :   2017        -   Last DPS year                                                       #
#############################################################################################################


def homes_delivery():

    logging.basicConfig(filename='hpi_logs.log', level=logging.DEBUG, format='%(asctime)s %(levelname)s : %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.info("Started Executing '5_homes_delivery_hpi.py' script")

    configparser = ConfigParser.RawConfigParser()
    config_filepath = r'../docs/credentials.cfg'
    configparser.read(config_filepath)

    user = configparser.get('aws2-config', 'user')
    pswd = configparser.get('aws2-config', 'pswd')
    host = configparser.get('aws2-config', 'hostdps')
    databs_hpis = configparser.get('aws2-config', 'databs_hpis')

    conn = MySQLdb.connect(host=host, user=user, passwd=pswd)
    cursor = conn.cursor()
    logging.captureWarnings(cursor)

    # Date manipulations
    now = datetime.now()
    run_datetime = now + dateutil.relativedelta.relativedelta(months=-1)
    run_date = run_datetime.strftime('%Y%m30')
    last_dps_datetime = now + dateutil.relativedelta.relativedelta(months=-2)
    last_dps_ym = last_dps_datetime.strftime('%Y%m')
    last_dps_month = last_dps_datetime.strftime('%m')
    last_dps_year = last_dps_datetime.strftime('%Y')

    # Creating table for Homes delivery
    drop_table = "DROP TABLE IF EXISTS " + databs_hpis + ".szhpi_monthly_increment_" + run_date + ";"
    cursor.execute(drop_table)
    conn.commit()

    check_table_existance("szhpi_monthly_history", databs_hpis, cursor, logging)
    create_mon_incr = """CREATE TABLE """ + databs_hpis + """.szhpi_monthly_increment_""" + run_date + """
                                (PRIMARY KEY (tract_geoid))
                                SELECT tract_geoid, val_""" + last_dps_ym + """ hpi
                                FROM """ + databs_hpis + """.szhpi_monthly_history;"""
    cursor.execute(create_mon_incr)
    conn.commit()
    logging.info("Table 'szhpi_monthly_increment_" + run_date + "' created '" + databs_hpis + "' in Database")

    # Creating BackUp Table for 'szhpi_annual_history'
    bkp_timestamp = datetime.now().strftime('%Y%m')
    create_annual_bkp = """CREATE TABLE IF NOT EXISTS """ + databs_hpis + """.szhpi_annual_history_bkp_""" + bkp_timestamp + """
                            SELECT * FROM """ + databs_hpis + """.szhpi_annual_history;"""
    cursor.execute(create_annual_bkp)
    conn.commit()
    logging.info("Backup Table 'szhpi_annual_history_bkp_" + bkp_timestamp + "' for 'szhpi_annual_history' created")
    
    # Updating 'szhpi_annual_history'
    update_annual_hist1 = "UPDATE " + databs_hpis + ".szhpi_annual_history SET val_" + last_dps_year + "=NULL;"
    cursor.execute(update_annual_hist1)
    conn.commit()

    val_list = []
    for mon_num in range(1, int(last_dps_month)+1):
        val_list.append('s.val_'+last_dps_year+(str(mon_num)).zfill(2))
    val_avg_string = '+'.join(val_list)

    update_annual_hist2 = """UPDATE """ + databs_hpis + """.szhpi_annual_history t, """ + databs_hpis + """.szhpi_monthly_history s
                                SET t.val_""" + last_dps_year + """=""" + val_avg_string + """/""" + last_dps_month + """
                                WHERE t.tract_geoid=s.tract_geoid;"""
    cursor.execute(update_annual_hist2)
    conn.commit()
    logging.info("Updated Table 'szhpi_annual_history' in '" + databs_hpis + "' Database")

    logging.info("Verification of output tables started")
    # Verifying table 'szhpi_monthly_increment_[run_date]' data
    check_null = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_increment_""" + run_date + """
                        WHERE tract_geoid IS NULL OR
                        hpi IS NULL OR
                        tract_geoid = '' OR
                        hpi = '';"""
    logging.info("Test Case #1 : NULL or Empty values check")
    cursor.execute(check_null)
    if cursor.fetchone()[0] != 0:
        raise Exception("Null or Empty value exists")
    else:
        logging.info("Test Case #1 : Passed")

    check_tract = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_increment_""" + run_date + """
                        WHERE tract_geoid NOT REGEXP '[0-9]' OR tract_geoid REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #2 : 'tract_geoid' values check")
    cursor.execute(check_tract)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid 'tract_geoid' value exists")
    else:
        logging.info("Test Case #2 : Passed")

    check_hpi = """SELECT count(*) FROM """ + databs_hpis + """.szhpi_monthly_increment_""" + run_date + """
                        WHERE hpi NOT REGEXP '[0-9]' OR hpi NOT BETWEEN 1 AND 500 OR hpi REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #3 : 'hpi' values check")
    cursor.execute(check_hpi)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid 'hpi' value exists")
    else:
        logging.info("Test Case #3 : Passed")

    logging.info("Verification of output tables completed")
    conn.close()

    logging.info("Completed Execution of '5_homes_delivery_hpi.py' script")

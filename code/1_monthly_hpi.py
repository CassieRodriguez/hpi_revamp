#!/usr/bin/python
from datetime import datetime
import csv
import ConfigParser
import MySQLdb
import pandas as pd
import os
import logging
import sys

#############################################################################################################
# Script has two functions:                                                                                 #
#      -monthly_hpi           :     creating 'hpi_month_fhfa_state_division' and                            #
#                                   'hpi_month_fhfa_state_division_horizontal' table                        #
#      -check_table_existance :     to check if table exists, used by all HPI scripts                       #
#                                                                                                           #
#   curr_year       :   18          -   Current year(only year number of 2018)                              #
#   curr_ym         :   201802      -   Current year and month when delivery happens                        #
#############################################################################################################


# Start the monthly hpi script run from this script
def check_table_existance(table, database, cursor, logger):
    check_table = """SELECT COUNT(*)
                FROM information_schema.tables 
                WHERE table_schema = '""" + database + """' 
                AND table_name = '""" + table + """';"""
    cursor.execute(check_table)
    if cursor.fetchone()[0] == 0:
        logger.error("Required Table '" + table + "' is not found in '" + database + "' Database...EXITING!")
        raise ValueError("Table '" + database + "." + table + "' doesn't exist")


def monthly_hpi():
    logging.basicConfig(filename='hpi_logs.log', level=logging.DEBUG, format='%(asctime)s %(levelname)s : %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.info("Started Executing '1_monthly_hpi.py' script")

    configparser = ConfigParser.RawConfigParser()
    config_filepath = r'../docs/credentials.cfg'
    configparser.read(config_filepath)

    user = configparser.get('aws2-config', 'user')
    pswd = configparser.get('aws2-config', 'pswd')
    host = configparser.get('aws2-config', 'hostdps')
    databs = configparser.get('aws2-config', 'databs')

    db = MySQLdb.connect(host=host, user=user, passwd=pswd)
    cursor = db.cursor()
    logging.captureWarnings(cursor)

    now = datetime.now()
    curr_y = now.strftime('%Y')
    curr_ym = now.strftime('%Y%m')

    check_table_existance("fhfa_" + curr_ym + "_hpi_monthly", databs, cursor, logging)
    select_hpi_mon = "SELECT month,year,hpi,vid FROM " + databs + ".fhfa_" + curr_ym + "_hpi_monthly WHERE sa not in ('NSA')"
    cursor.execute(select_hpi_mon)
    data = cursor.fetchall()

    f = open('fhfa_hpi_mon.csv', 'w')
    for t in data:
        line = ','.join(str(x) for x in t)
        f.write(line + '\n')
    f.close()

    # Database result sanitation by removing unwanted lines and formatting as well
    with open('fhfa_hpi_mon.csv') as sample, open('fhfa_hpi_mon_final.csv', 'w') as output:
        reader = csv.reader(sample)
        writer = csv.writer(output)
        writer.writerow(['Month', 'Year', 'HPI', 'Div'])
        for row in reader:
            if not row[3].startswith('None'):
                writer.writerow(row)

    today = datetime.today()
    t1 = []
    for state_fips in (1,2,4,5,6,8,9,10,11,12,13,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,44,45,46,47,48,49,50,51,53,54,55,56):
        for year in xrange(1993, today.year + 1):
            for month in xrange(1,13):
                yearMonth1 = year, month, state_fips
                t1.append(yearMonth1)
                # break out of month loop if this month is reached
                if (year, month) == (today.year, today.month):
                    break

    f = open('year_month.csv', 'w')
    for t in t1:
        line = ','.join(str(x) for x in t)
        f.write(line + '\n')
    f.close()

    with open('year_month.csv') as sample, open('hpi_mon_state_div.csv','w') as output:
        reader = csv.reader(sample)
        writer = csv.writer(output)
        writer.writerow(['Year', 'Month', 'State_fips', 'Div','HPI'])
        # div_id = int()
        for row in reader:
            if row:
                div_id = int()
                HPI = int()
                state_fips = row[2]
                if state_fips in ('9','23','25','33','44','50'):
                    div_id = 1
                elif state_fips in ('34','36','42'):
                    div_id = 2
                elif state_fips in ('17','18','26','39','55'):
                    div_id = 3
                elif state_fips in ('19','20','27','29','31','38','46'):
                    div_id = 4
                elif state_fips in ('10','11','12','13','24','37','45','51','54'):
                    div_id = 5
                elif state_fips in ('1','21','28','47'):
                    div_id = 6
                elif state_fips in ('5','22','40','48'):
                    div_id = 7
                elif state_fips in ('4','8','16','30','32','35','49','56'):
                    div_id = 8
                elif state_fips in ('2','6','15','41','53'):
                    div_id = 9
            writer.writerow([row[0], row[1], row[2], div_id, HPI])

    # Merging both the csvs with common lines and adding HPI column
    df_a = pd.read_csv('fhfa_hpi_mon_final.csv')
    df_b = pd.read_csv('hpi_mon_state_div.csv')
    merged_df = df_a.merge(df_b.drop('HPI', 1))

    merged_df.to_csv('hpi_mon_state_div.csv', encoding='utf-8', index=False)
    logging.info("CSV file 'hpi_mon_state_div.csv' created")

    drop_table = "DROP TABLE IF EXISTS " + databs + ".hpi_month_fhfa_state_division_" + curr_ym
    cursor.execute(drop_table)
    db.commit()

    create_mon_state_div = ("""CREATE TABLE """ + databs + ".hpi_month_fhfa_state_division_" + curr_ym + """ (
                    Month int(2),
                    Year int(4),
                    hpi double,
                    Divi int(2),
                    State_fips INT(3)
                    );""")
    cursor.execute(create_mon_state_div)
    db.commit()
    logging.info("Table 'hpi_month_fhfa_state_division_" + curr_ym + "' created in '" + databs + "' Database")

    load_mon_state_div = ("""LOAD DATA LOCAL INFILE
                    'hpi_mon_state_div.csv'
                    INTO TABLE
                    """ + databs + ".hpi_month_fhfa_state_division_" + curr_ym + """
                    CHARACTER SET utf8
                    FIELDS TERMINATED BY ','
                    LINES TERMINATED BY '\n'
                    IGNORE 1 LINES
                    ;""")

    cursor.execute(load_mon_state_div)
    db.commit()
    logging.info(
        "Load from CSV file 'hpi_mon_state_div.csv' to Table 'hpi_month_fhfa_state_division_" + curr_ym + "' completed")

    # Final output.csv is required for uploading into hpi_month_fhfa_state_division table, So creating another csv for the horizontal table edits.
    pre_horizontal_df = pd.read_csv('hpi_mon_state_div.csv')
    pre_horizontal_df['period'] = merged_df[['Year', 'Month']].dot([100, 1])
    pre_horizontal_df['period'] = "val_"+pre_horizontal_df['period'].astype(str)
    horizontal_df = pre_horizontal_df.pivot_table(index='State_fips', columns='period', values='HPI', aggfunc='mean')
    horizontal_df = horizontal_df.reset_index().rename_axis(None, axis=1)
    # DROPPING COLUMNS BETWEEN VAL_199301 AND VAL_199912
    horizontal_df = horizontal_df.drop(horizontal_df.columns.to_series()["val_199301":"val_199912"], axis=1)
    horizontal_df.to_csv('hpi_mon_state_div_horiz.csv', encoding='utf-8', index=False)
    logging.info("CSV file 'hpi_mon_state_div_horiz.csv' created")
    # Adding the state code (AL,AK etc to the horizontal data)
    df_a = pd.read_csv('hpi_mon_state_div_horiz.csv')
    df_b = pd.DataFrame([[1, "AL"],[2, "AK"],[4, "AZ"],[5, "AR"],[6, "CA"],[8, "CO"],[9, "CT"],[10, "DE"],[11, "DC"],[12, "FL"],[13, "GA"],[15, "HI"],[16, "ID"],[17, "IL"],[18, "IN"],[19, "IA"],[20, "KS"],[21, "KY"],[22, "LA"],[23, "ME"],[24, "MD"],[25, "MA"],[26, "MI"],[27, "MN"],[28, "MS"],[29, "MO"],[30, "MT"],[31, "NE"],[32, "NV"],[33, "NH"],[34, "NJ"],[35, "NM"],[36, "NY"],[37, "NC"],[38, "ND"],[39, "OH"],[40, "OK"],[41, "OR"],[42, "PA"],[44, "RI"],[45, "SC"],[46, "SD"],[47, "TN"],[48, "TX"],[49, "UT"],[50, "VT"],[51, "VA"],[53, "WA"],[54, "WV"],[55, "WI"],[56, "WY"]], columns=["State_fips", "state_code"])

    merged_df = pd.merge(df_a, df_b, on="State_fips", how="left")
    merged_df.stat_fips = merged_df.State_fips.fillna(0).astype(int)

    merged_df = pd.concat([merged_df], axis=1)
    # Dropping the last column and adding it to the front 2nd column
    mid = merged_df['state_code']
    merged_df.drop(labels=['state_code'], axis=1, inplace=True)
    merged_df.insert(1, 'state_code', mid)

    merged_df.to_csv('hpi_mon_state_div_horiz.csv', encoding='utf-8', index=False)
    logging.info("CSV file 'hpi_mon_state_div_horiz.csv' modified and re-created")

    # Removing intermediate files created at different stages
    os.remove('fhfa_hpi_mon_final.csv')
    os.remove('year_month.csv')
    os.remove('fhfa_hpi_mon.csv')
    logging.info("Removed intermediate CSV files")

    end_date = curr_ym[:4] + "-" + curr_ym[4:]
    mon_cols = [i.strftime("val_%Y%m") for i in pd.date_range(start="2000-01", end=end_date, freq='MS')] # till current month
    mon_cols = mon_cols[:-3] # neglecting last three months using splicing
    mon_col_list = map(lambda x: x + " DEC(8,4)", mon_cols)

    drop_table_horiz = ("DROP TABLE IF EXISTS " + databs + ".hpi_month_fhfa_state_division_" + curr_ym + "_horizontal")
    cursor.execute(drop_table_horiz)
    db.commit()

    create_mon_state_div_horiz = """CREATE TABLE """ + databs + """.hpi_month_fhfa_state_division_""" + curr_ym + """_horizontal
                (PRIMARY KEY (State_fips),
                State_fips INT(3),
                state_code varchar(3),
                """ + ",".join(mon_col_list) + """);"""

    cursor.execute(create_mon_state_div_horiz)
    db.commit()
    logging.info(
        "Table 'hpi_month_fhfa_state_division_" + curr_ym + "_horizontal' created in '" + databs + "' Database")
    
    load_mon_state_div_horiz = ("""LOAD DATA LOCAL INFILE
                'hpi_mon_state_div_horiz.csv'
                INTO TABLE
                """ + databs + ".hpi_month_fhfa_state_division_" + curr_ym + """_horizontal
                CHARACTER SET utf8
                FIELDS TERMINATED BY ','
                LINES TERMINATED BY '\n'
                IGNORE 1 LINES;""")

    cursor.execute(load_mon_state_div_horiz)
    db.commit()
    logging.info(
        "Load from CSV file 'hpi_mon_state_div_horiz.csv' to Table 'hpi_month_fhfa_state_division_" + curr_ym + "_horizontal' completed")

    os.remove("hpi_mon_state_div.csv")
    os.remove("hpi_mon_state_div_horiz.csv")
    logging.info("Removed intermediate CSV files")

    logging.info("Verification of output tables started")
    # Verifying table 'hpi_month_fhfa_state_division_[curr_ym]' data
    check_null = """SELECT count(*) FROM """ + databs + """.hpi_month_fhfa_state_division_""" + curr_ym + """
                WHERE Month IS NULL OR
                Year IS NULL OR
                HPI IS NULL OR
                Divi IS NULL OR
                State_fips IS NULL OR 
                Year = '' OR
                HPI = '' OR
                Divi = '' OR
                State_fips = '';"""
    logging.info("Test Case #1 : NULL or Empty check")
    cursor.execute(check_null)
    if cursor.fetchone()[0] != 0:
        raise Exception("Null or Empty value exists")
    else:
        logging.info("Test Case #1 : Passed")

    check_month = """SELECT count(*) FROM """ + databs + """.hpi_month_fhfa_state_division_""" + curr_ym + """
                WHERE Month NOT BETWEEN 1 AND 12 OR Month REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #2 : 'Month' values check")
    cursor.execute(check_month)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid Month value exists")
    else:
        logging.info("Test Case #2 : Passed")

    check_year = """SELECT count(*) FROM """ + databs + """.hpi_month_fhfa_state_division_""" + curr_ym + """
                    WHERE Year NOT BETWEEN 1993 AND """ + curr_y + """ OR Year REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #3 : 'Year' values check")
    cursor.execute(check_year)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid Year value exists")
    else:
        logging.info("Test Case #3 : Passed")

    check_hpi = """SELECT count(*) FROM """ + databs + """.hpi_month_fhfa_state_division_""" + curr_ym + """
                            WHERE hpi NOT BETWEEN 1 AND 500 OR hpi REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #4 : 'hpi' values check")
    cursor.execute(check_hpi)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid HPI value exists")
    else:
        logging.info("Test Case #4 : Passed")

    check_div = """SELECT count(*) FROM """ + databs + """.hpi_month_fhfa_state_division_""" + curr_ym + """
                        WHERE Divi NOT BETWEEN 1 AND 9 OR Divi REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #5 : 'div' values check")
    cursor.execute(check_div)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid Divi value exists")
    else:
        logging.info("Test Case #5 : Passed")

    check_sfips = """SELECT count(*) FROM """ + databs + """.hpi_month_fhfa_state_division_""" + curr_ym + """
                            WHERE State_fips NOT IN (1,2,4,5,6,8,9,10,11,12,13,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,44,45,46,47,48,49,50,51,53,54,55,56);"""
    logging.info("Test Case #6 : 'State_fips' values check")
    cursor.execute(check_sfips)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid State_fips value exists")
    else:
        logging.info("Test Case #6 : Passed")

    # Verifying table 'hpi_month_fhfa_state_division_[curr_ym]_horizontal' data
    check_null = """SELECT count(*) FROM """ + databs + """.hpi_month_fhfa_state_division_""" + curr_ym + """_horizontal
                    WHERE State_fips IS NULL OR
                    state_code  IS NULL OR
                    State_fips = '' OR
                    state_code = '';"""
    logging.info("Test Case #7 : NULL or Empty check")
    cursor.execute(check_null)
    if cursor.fetchone()[0] != 0:
        raise Exception("NULL or Empty value exists")
    else:
        logging.info("Test Case #7 : Passed")

    check_sfips = """SELECT count(*) FROM """ + databs + """.hpi_month_fhfa_state_division_""" + curr_ym + """_horizontal
                                WHERE State_fips NOT IN (1,2,4,5,6,8,9,10,11,12,13,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,44,45,46,47,48,49,50,51,53,54,55,56);"""
    logging.info("Test Case #8 : 'State_fips' values check")
    cursor.execute(check_sfips)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid State_fips value exists")
    else:
        logging.info("Test Case #8 : Passed")

    check_scode = """SELECT count(*) FROM """ + databs + """.hpi_month_fhfa_state_division_""" + curr_ym + """_horizontal
                                    WHERE state_code NOT IN ("AL","AK","AZ","AR","CA","CO","CT","DE","DC","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY","AL","AK","AZ","AR","CA","CO","CT","DE","DC","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY");"""
    logging.info("Test Case #9 : 'state_code' values check")
    cursor.execute(check_scode)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid state_code value exists")
    else:
        logging.info("Test Case #9 : Passed")

    check_mon_list = map(lambda x: x + " NOT BETWEEN 1 AND 500", mon_cols)
    check_val = """SELECT count(*) FROM """ + databs + """.hpi_month_fhfa_state_division_""" + curr_ym + """_horizontal
                                WHERE """ + ' OR '.join(check_mon_list) + """;"""
    logging.info("Test Case #10 : Columns ('val_[#]') values check")
    cursor.execute(check_val)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid 'val_[#]' value exists")
    else:
        logging.info("Test Case #10 : Passed")

    logging.info("Verification of output tables completed")
    db.close()

    logging.info("Completed Execution of '1_monthly_hpi.py' script")

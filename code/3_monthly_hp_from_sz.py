import ConfigParser
from datetime import datetime
import dateutil.relativedelta
import MySQLdb
from math import pow
import os
import logging
import pandas as pd
import numpy as np

#############################################################################################################
# Script has one function:                                                                                  #
#      -monthly_hp_from_sz           :     creating table 'hpi_year_sz_tract' using data from               #
#                                          'hpi_coeff_g[#]' tables                                          #
#                                                                                                           #
#   run_date     :   20180130    -   Latest DPS date                                                        #
#############################################################################################################


def monthly_hp_from_sz():
    logging.basicConfig(filename='hpi_logs.log', level=logging.DEBUG, format='%(asctime)s %(levelname)s : %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.info("Started Executing '3_monthly_hp_from_sz.py' script")
    configparser = ConfigParser.RawConfigParser()
    config_filepath = r'../docs/credentials.cfg'
    configparser.read(config_filepath)

    user = configparser.get('aws2-config', 'user')
    pswd = configparser.get('aws2-config', 'pswd')
    host = configparser.get('aws2-config', 'hostdps')
    databs_hpis = configparser.get('aws2-config', 'databs_hpis')

    conn = MySQLdb.connect(host=host, user=user, passwd=pswd)
    cursor = conn.cursor()
    logging.captureWarnings(cursor)

    run_datetime = datetime.now() + dateutil.relativedelta.relativedelta(months=-1)
    run_date = run_datetime.strftime('%Y%m30')
    nxt_year = int(run_datetime.strftime('%Y'))+1
    table_nums = map(str, range(1, 9))   # Table name suffix

    def check_table_exists(table):
        check_table = "SELECT COUNT(*) FROM information_schema.tables WHERE table_name = 'hpi_coeff_g" + table + "';"
        cursor.execute(check_table)
        if cursor.fetchone()[0] == 0:
            logging.warning("Table hpi_coeff_g" + table + " does not exist...SKIPPING!")
            return False
        return True

    df = pd.DataFrame()
    year_list = ["val_%02d" % x for x in range(1993, nxt_year)]

    for table_num in table_nums:
        if check_table_exists(table_num):
            select_table = "select rawid, region, geotype, geoid as tract_geoid, " + ','.join(year_list) + " from hpi_" + run_date + ".hpi_coeff_g" + table_num + ";"
            df = df.append(pd.read_sql(select_table, conn), ignore_index=True)

    logging.info("Fetched data from all 'hpi_coeff_g[1-9]' tables")

    df.loc[(df['val_2001'].notna()) & (df['val_2000'].isna()), 'val_2000'] = (0.91516427 * df['val_2001'])
    df.loc[(df['val_2000'].notna()) & (df['val_1999'].isna()), 'val_1999'] = (0.914234 * df['val_2000'])

    for year in range(1993, nxt_year):
        year = str(year)
        df.loc[(df['val_' + year] == 0), 'val_' + year] = np.nan

    df.drop(df[df['val_2000'] > df['val_1999'] * 1.5].index, inplace=True)
    df.drop(df[df['val_2000'] < df['val_1999'] * 0.5].index, inplace=True)

    year_pow_dict1 = {'2001': pow(1.5, 2),
                      '2002': pow(1.5, 3),
                      '2003': pow(1.5, 4),
                      '2004': pow(1.3, 5),
                      '2005': pow(1.3, 6),
                      '2006': pow(1.3, 7),
                      '2007': pow(1.3, 8),
                      '2008': pow(1.2, 9),
                      '2009': pow(1.2, 10),
                      '2010': pow(1.2, 11),
                      '2011': pow(1.2, 12)}

    year_pow_dict1.update({str(year): pow(1.2, 13) for year in range(2012, nxt_year)})

    year_pow_dict2 = {'2001': pow(0.5, 2),
                      '2002': pow(0.5, 3),
                      '2003': pow(0.5, 4),
                      '2004': pow(0.3, 5),
                      '2005': pow(0.3, 6),
                      '2006': pow(0.3, 7),
                      '2007': pow(0.3, 8),
                      '2008': pow(0.2, 9),
                      '2009': pow(0.2, 10),
                      '2010': pow(0.2, 11),
                      '2011': pow(0.2, 12)}

    year_pow_dict2.update({str(year): pow(0.2, 13) for year in range(2012, nxt_year)})

    for year, pow_val in year_pow_dict1.items():
        l_year = str(int(year) - 1)
        df.drop(df[(df['val_' + year] > df['val_' + l_year] * 1.5) | (df['val_' + year] > df['val_1999'] * pow_val)].index, inplace=True)

    for year, pow_val in year_pow_dict2.items():
        l_year = str(int(year) - 1)
        df.drop(df[(df['val_' + year] < df['val_' + l_year] * 0.5) | (df['val_' + year] < df['val_1999'] * pow_val)].index, inplace=True)

    for year in year_list:
        df[year] = (100 * df[year] / df['val_2000'])

    df['val_2000'] = 100

    if df.isnull().values.any():
        raise Exception(
            "Table 'hpi_year_sz_tract_" + run_date + "' must not contain NULL values")

    df.to_csv('hpi_year_sz_tract.csv', encoding='utf-8', index=False)
    logging.info("Calculated data written to CSV file 'hpi_year_sz_tract.csv'")

    drop_table = "DROP TABLE IF EXISTS " + databs_hpis + ".hpi_year_sz_tract_" + run_date + ";"
    cursor.execute(drop_table)

    year_col_list = map(lambda year: year + " decimal(20,12)", year_list)

    create_table = """CREATE TABLE """ + databs_hpis + """.hpi_year_sz_tract_""" + run_date + """(
                rawid int(10),
                region tinyint(2),
                geotype tinyint(2),
                tract_geoid bigint(12),""" + ','.join(year_col_list) + """);"""

    cursor.execute(create_table)
    conn.commit()
    logging.info("Table 'hpi_year_sz_tract_" + run_date + "' created in '" + databs_hpis + "' Database")

    load_data = """LOAD DATA LOCAL INFILE
                'hpi_year_sz_tract.csv'
                INTO TABLE
                """ + databs_hpis + """.hpi_year_sz_tract_""" + run_date + """
                CHARACTER SET utf8
                FIELDS TERMINATED BY ','
                LINES TERMINATED BY '\n'
                IGNORE 1 LINES; """

    cursor.execute(load_data)
    conn.commit()
    logging.info("Load from CSV file 'hpi_year_sz_tract.csv' to Table 'hpi_year_sz_tract_" + run_date + "' completed")

    #os.remove("hpi_year_sz_tract.csv")
    logging.info("Removed intermediate CSV files")

    del_data = """DELETE FROM 
                """ + databs_hpis + ".hpi_year_sz_tract_" + run_date + """
                WHERE tract_geoid IN (SELECT tract_geoid FROM """ + databs_hpis + """.bad_tracts);"""
    cursor.execute(del_data)
    conn.commit()
    logging.info("Deletion of non-required 'tract_geoid's from table 'hpi_year_sz_tract_" + run_date + "' is completed")

    logging.info("Verification of output tables started")
    # Verifying table 'hpi_year_sz_tract_[run_date]' data
    check_null = """SELECT count(*) FROM """ + databs_hpis + """.hpi_year_sz_tract_""" + run_date + """
                    WHERE rawid IS NULL OR
                    region IS NULL OR
                    geotype IS NULL OR
                    tract_geoid IS NULL OR
                    rawid = '' OR
                    tract_geoid = '';"""
    logging.info("Test Case #1 : NULL or Empty value check")
    cursor.execute(check_null)
    if cursor.fetchone()[0] != 0:
        raise Exception("Null or Empty value exists")
    else:
        logging.info("Test Case #1 : Passed")

    check_rawid = """SELECT count(*) FROM """ + databs_hpis + """.hpi_year_sz_tract_""" + run_date + """
                    WHERE rawid NOT REGEXP '[0-9]' OR rawid REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #2 : 'rawid' value check")
    cursor.execute(check_rawid)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid 'rawid' value exists")
    else:
        logging.info("Test Case #2 : Passed")

    check_region = """SELECT count(*) FROM """ + databs_hpis + """.hpi_year_sz_tract_""" + run_date + """
                      WHERE region NOT REGEXP '[0-9]' OR region REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #3 : 'region' values check")
    cursor.execute(check_region)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid 'region' value exists")
    else:
        logging.info("Test Case #3 : Passed")

    check_geotype = """SELECT count(*) FROM """ + databs_hpis + """.hpi_year_sz_tract_""" + run_date + """
                       WHERE geotype NOT REGEXP '[0-9]' OR geotype REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #4 : 'geotype' values check")
    cursor.execute(check_geotype)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid 'geotype' value exists")
    else:
        logging.info("Test Case #4 : Passed")

    check_tract = """SELECT count(*) FROM """ + databs_hpis + """.hpi_year_sz_tract_""" + run_date + """
                           WHERE tract_geoid NOT REGEXP '[0-9]' OR tract_geoid REGEXP '[a-zA-Z~`!@#$%^&*()_=+{}|\:;"<>?/\-]';"""
    logging.info("Test Case #5 : 'tract_geoid' values check")
    cursor.execute(check_tract)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid 'tract_geoid' value exists")
    else:
        logging.info("Test Case #5 : Passed")

    check_year_list = map(lambda x: x + " NOT BETWEEN 1 AND 1500", year_list)
    check_val = """SELECT count(*) FROM """ + databs_hpis + """.hpi_year_sz_tract_""" + run_date + """
                                    WHERE """ + ' OR '.join(check_year_list) + """;"""
    logging.info("Test Case #6 : Columns (val_[#]) values check")
    cursor.execute(check_val)
    if cursor.fetchone()[0] != 0:
        raise Exception("Invalid val_[#] value exists")
    else:
        logging.info("Test Case #6 : Passed")

    logging.info("Verification of output tables completed")
    conn.close()

    logging.info("Completed Execution of '3_monthly_hp_from_sz.py' script")

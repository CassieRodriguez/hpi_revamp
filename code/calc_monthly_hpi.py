import os
import time
import logging
import traceback
import subprocess
from datetime import datetime


if __name__ == '__main__':
    # Backing up old logs if exists
    if not os.path.isdir('old_logs'):
        os.mkdir('old_logs')

    if os.path.isfile('hpi_logs.log'):
        # Getting create time of last log file
        create_time = time.ctime(os.path.getatime("hpi_logs.log"))
        log_stamp = datetime.strptime(create_time, "%a %b %d %H:%M:%S %Y").strftime("%Y%m%d%H%M%S")

        os.rename("hpi_logs.log", "old_logs/hpi_logs_" + log_stamp + ".log")

    hpi_script_1 = __import__('1_monthly_hpi')
    hpi_script_2 = __import__('2_monthly_hpi_cbsas_horizontal')
    hpi_script_3 = __import__('3_monthly_hp_from_sz')
    hpi_script_4 = __import__('4_final_monthly_hpi')
    hpi_script_5 = __import__('5_homes_delivery_hpi')
    hpi_script_6 = __import__('6_szhpi_higher_geo')
    hpi_script_7 = __import__('7_szhpi_higher_geo_state')

    logging.basicConfig(filename='hpi_logs.log', level=logging.DEBUG, format='%(asctime)s %(levelname)s : %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.info("Execution of HPI Monthly Pipeline started")
    print "Execution of HPI Monthly Pipeline started at: ", time.strftime('%H:%M:%S   %s'), "\n"
    try:
        #hpi_script_1.monthly_hpi()
        #hpi_script_2.monthly_hpi_cbsas_horiz()
        #hpi_script_3.monthly_hp_from_sz()
        #hpi_script_4.final_monthly_hpi()
        #hpi_script_5.homes_delivery()
        #subprocess.call("./monthly_hpi_r.sh")
        hpi_script_6.szhpi_higher_geo()
        hpi_script_7.szhpi_higher_geo_state()
        hpi_script_6.patch()

    except Exception:
        logging.error(traceback.format_exc())
        logging.error("Execution of HPI Monthly Pipeline is NOT completed due to error occurred...EXITING!")
        exit("Execution of HPI Monthly Pipeline is NOT completed due to error occurred...EXITING!")

    logging.info("Execution of HPI Monthly Pipeline completed")
    print "Execution of HPI Monthly Pipeline completed at: ", time.strftime('%H:%M:%S   %s')
